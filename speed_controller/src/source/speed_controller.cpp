#include "speed_controller.h"

//Constructor
SpeedController::SpeedController()
{
    std::cout << "Constructor: SpeedController" << std::endl;

}

//Destructor
SpeedController::~SpeedController() {}



bool SpeedController::readConfigs(std::string configFile)
{

    try
    {


    XMLFileReader my_xml_reader(configFile);


    /*********************************  Speed X Controller ( from Dx to Pitch ) ************************************************/

    // Gain
    pid_dx2pitch_kp = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dx2Pitch:Gain:Kp");
    pid_dx2pitch_ki = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dx2Pitch:Gain:Ki");
    pid_dx2pitch_kd = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dx2Pitch:Gain:Kd");
    pid_dx2pitch_enablesat = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dx2Pitch:Saturation:enable_saturation");
    pid_dx2pitch_satmax = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dx2Pitch:Saturation:SatMax");
    pid_dx2pitch_satmin = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dx2Pitch:Saturation:SatMin");
    pid_dx2pitch_enableantiwp = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dx2Pitch:Anti_wind_up:enable_anti_wind_up");
    pid_dx2pitch_kw = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dx2Pitch:Anti_wind_up:Kw");


    /*********************************  Speed Y Controller ( from Dy to Roll ) **************************************************/

    // Gain
    pid_dy2roll_kp = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dy2Roll:Gain:Kp");
    pid_dy2roll_ki = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dy2Roll:Gain:Ki");
    pid_dy2roll_kd = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dy2Roll:Gain:Kd");
    pid_dy2roll_enablesat = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dy2Roll:Saturation:enable_saturation");
    pid_dy2roll_satmax = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dy2Roll:Saturation:SatMax");
    pid_dy2roll_satmin = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dy2Roll:Saturation:SatMin");
    pid_dy2roll_enableantiwp = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dy2Roll:Anti_wind_up:enable_anti_wind_up");
    pid_dy2roll_kw = my_xml_reader.readDoubleValue("Speed_Controller:PID_Dy2Roll:Anti_wind_up:Kw");


    /*********************************  Max Speed Reference *************************************/
    pid_speed_max=my_xml_reader.readDoubleValue("Speed_Controller:Vmax");
    pid_speed_enablemax=my_xml_reader.readDoubleValue("Speed_Controller:enablemax");


    }


    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }


    return true;
}

void SpeedController::setUp()
{

    ros::param::get("~stackPath", stackPath);
    if ( stackPath.length() == 0)
    {
        stackPath = "$(env AEROSTACK_STACK)";
    }
    ros::param::get("~droneId", idDrone);
    ros::param::get("~spd_config_file", spdconfigFile);
    if ( spdconfigFile.length() == 0)
    {
        spdconfigFile="speed_controller.xml";
    }

    bool readConfigsBool = readConfigs(stackPath+"/configs/drone"+cvg_int_to_string(idDrone)+"/"+spdconfigFile);
    if(!readConfigsBool)
    {
        std::cout << "Error init"<< std::endl;
        return;
    }
    std::cout << "Constructor: SpeedController...Exit" << std::endl;

}

void SpeedController::start()
{

    // Reset PID
    PID_Dx2Pitch.reset();
    PID_Dy2Roll.reset();

}


void SpeedController::stop()
{

}


void SpeedController::setFeedback(float velx, float vely){
    PID_Dx2Pitch.setFeedback(velx);
    PID_Dy2Roll.setFeedback(vely);
}
void SpeedController::setReference(float ref_velx, float ref_vely){

    if (pid_speed_enablemax){
        float vmax=sqrt(pow(ref_velx,2)+pow(ref_vely,2));
        if (vmax > pid_speed_max){
            ref_velx=ref_velx/vmax*pid_speed_max;
            ref_vely=ref_vely/vmax*pid_speed_max;
        }
    }

    PID_Dx2Pitch.setReference(ref_velx);
    PID_Dy2Roll.setReference(ref_vely);
}

void SpeedController::getOutput(float *pitchb, float *rollb, float yaw){

    /*********************************  Speed X Controller ( from Dx to Pitch ) ************************************************/

    PID_Dx2Pitch.setGains(pid_dx2pitch_kp,pid_dx2pitch_ki,pid_dx2pitch_kd);
    PID_Dx2Pitch.enableMaxOutput(pid_dx2pitch_enablesat,pid_dx2pitch_satmin,pid_dx2pitch_satmax);
    PID_Dx2Pitch.enableAntiWindup(pid_dx2pitch_enableantiwp,pid_dx2pitch_kw);

    pitch = -PID_Dx2Pitch.getOutput();


    /*********************************  Speed Y Controller ( from Dy to Roll ) **************************************************/

    PID_Dy2Roll.setGains(pid_dy2roll_kp,pid_dy2roll_ki,pid_dy2roll_kd);
    PID_Dy2Roll.enableMaxOutput(pid_dy2roll_enablesat,pid_dy2roll_satmin,pid_dy2roll_satmax);
    PID_Dy2Roll.enableAntiWindup(pid_dy2roll_enableantiwp,pid_dy2roll_kw);


    roll = PID_Dy2Roll.getOutput();


    /******************************** World to Body Transformation ********************************************/

    *pitchb =  pitch * cos (yaw) - roll * sin (yaw);
    *rollb  =  - pitch * sin (yaw) - roll * cos (yaw);

}

