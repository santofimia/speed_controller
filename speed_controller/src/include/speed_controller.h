#ifndef speed_controller
#define speed_controller

#include "pid_control.h"

/*!*************************************************************************************
 *  \class     speed_controller
 *
 *  \brief     Speed Controller PID
 *
 *  \details   This class is in charge of control speed in XY using PID class.
 *
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include <ros/ros.h>

#include "xmlfilereader.h"

class SpeedController
{
public:

  void setUp();

  void start();

  void stop();

  void getOutput(float *pitchb, float *rollb, float yaw);
  void setReference(float ref_velx, float ref_vely);
  void setFeedback(float velx, float vely);

  //! Read Config
  bool readConfigs(std::string configFile);

  //! Constructor. \details Same arguments as the ros::init function.
  SpeedController();

  //! Destructor.
  ~SpeedController();

private:

  PID PID_Dx2Pitch;
  PID PID_Dy2Roll;

  //! Configuration file variable
  int idDrone;               // Id Drone integer (number of the drone)
  std::string spdconfigFile;    // Config File String name
  std::string stackPath;     // Config File String aerostack path name

  // Output
  float pitch;
  float roll;

  // Gains

  float pid_dx2pitch_kp;
  float pid_dx2pitch_ki;
  float pid_dx2pitch_kd;
  bool pid_dx2pitch_enablesat;
  float pid_dx2pitch_satmax;
  float pid_dx2pitch_satmin;
  bool pid_dx2pitch_enableantiwp;
  float pid_dx2pitch_kw;


  float pid_dy2roll_kp;
  float pid_dy2roll_ki;
  float pid_dy2roll_kd;
  bool pid_dy2roll_enablesat;
  float pid_dy2roll_satmax;
  float pid_dy2roll_satmin;
  bool pid_dy2roll_enableantiwp;
  float pid_dy2roll_kw;


  float pid_speed_max;
  bool pid_speed_enablemax;

};
#endif
